/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    CircleTest.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Circle CPPUNIT test specification.
 * @see     http://sourceforge.net/projects/cppunit/files
 */

#ifndef CSC232_CIRCLE_TEST_H__
#define CSC232_CIRCLE_TEST_H__

#include <cmath>
#include <cppunit/extensions/HelperMacros.h>
#include "../release/Circle.h"

class CircleTest : public CPPUNIT_NS::TestFixture {
  CPPUNIT_TEST_SUITE(CircleTest);

  CPPUNIT_TEST(defaultCircleIsUnitCircle);
  CPPUNIT_TEST(initializedCircleHasExpectedRadius);
  CPPUNIT_TEST(defaultCircleHasExpectedPerimeter);
  CPPUNIT_TEST(initializedCircleHasExpectedPerimeter);
  CPPUNIT_TEST(defaultCircleHasExpectedArea);
  CPPUNIT_TEST(initializedCircleHasExpectedArea);
  CPPUNIT_TEST(defaultCircleHasExpectedCircumference);
  CPPUNIT_TEST(initializedCircleHasExpectedCircumference);
  CPPUNIT_TEST(setRadiusChangesRadiusAsExpected);
  
  CPPUNIT_TEST_SUITE_END();

public:
  CircleTest();
  virtual ~CircleTest();
  void setUp();
  void tearDown();

  /**
   * @brief Define PI in terms of trig initializedCircleHasExpectedPerimeternction.
   */
  static constexpr double PI{std::atan(1.0) * 4.0};

  /**
   * @brief Length of unit circle radius.
   */
  static constexpr csc232::length_unit UNIT_CIRCLE_RADIUS{1.0};

  /**
   * @brief Length of initialized circle radius.
   */
  static constexpr csc232::length_unit INIT_CIRCLE_RADIUS{2.5};

  /**
   * @brief Expected perimeter of a unit circle.
   */
  static constexpr csc232::length_unit UNIT_CIRCLE_PERIM{ 2.0 * PI * UNIT_CIRCLE_RADIUS};

  /**
   * @brief Expected perimeter of initialized circle.
   */ 
  static constexpr csc232::length_unit INIT_CIRCLE_PERIM{ 2.0 * PI * INIT_CIRCLE_RADIUS};

  /**
   * @brief Expected area of a unit circle.
   */
  static constexpr csc232::length_unit UNIT_CIRCLE_AREA{PI * UNIT_CIRCLE_RADIUS * UNIT_CIRCLE_RADIUS};

  /**
   * @brief Expected are of initialized circle.
   */
  static constexpr csc232::length_unit INIT_CIRCLE_AREA{PI * INIT_CIRCLE_RADIUS * INIT_CIRCLE_RADIUS};
  
  /**
   * @brief A margin of error used when comparing @code double @endcode
   *        values.
   */
  static constexpr csc232::length_unit DELTA{0.000001};
  
  /**
   * @brief The number of decimal places after the decimal point to print in
   *        any messages involving @code double @endcode values.
   */
  static const int MSG_PRECISION{4};
  
 private:
  /* Unit test methods */
  void defaultCircleIsUnitCircle();
  void initializedCircleHasExpectedRadius();
  void defaultCircleHasExpectedPerimeter();
  void initializedCircleHasExpectedPerimeter();
  void defaultCircleHasExpectedArea();
  void initializedCircleHasExpectedArea();
  void defaultCircleHasExpectedCircumference();
  void initializedCircleHasExpectedCircumference();
  void setRadiusChangesRadiusAsExpected();
  
  /* Test objects */
  csc232::Circle unitCircle;
  csc232::Circle initializedCircle{INIT_CIRCLE_RADIUS};
};

#endif // CSC232_CIRCLE_TEST_H__

