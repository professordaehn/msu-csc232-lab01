/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Circle.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <fillmein@missouristate.edu>
 *          Partner Name <partnerid@missouristate.edu>
 * @brief   Circle specification.
 */

#ifndef CSC232_SHAPE_H__
#define CSC232_SHAPE_H__

#include "Shape.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

  /**
   * @brief A concrete realization of the Shape interface.
   */
  class Circle : public Shape {
  public:
    /* Constructors */
    Circle();
    Circle(const length_unit radius);

    /* Getter/Setters */
    length_unit getRadius() const;
    void setRadius(const length_unit radius);

    /* Overrides */
    virtual length_unit getPerimeter() const override;
    virtual length_unit getArea() const override;

    /* Additional operations */
    length_unit getCircumference() const;

  private:
    length_unit m_radius;
  };
}

#endif // CSC232_SHAPE_H__
